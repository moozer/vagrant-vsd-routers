To use

1. "git clone" this repo
2. Provision using "vagrant up"
3. you should now have 2 virtualbox VMs: "routerA and routerB
4. To test the routers, use `ansible-playbook playbook.yml -i .vagrant/provisioners/ansible/inventory/vagrant_ansible_inventory -t test`

Expand and use

4. Create a new VM in the virtualbox GUI
    * Suggestions are to a simple debian or ubuntu
    * see e.g. [this guide](https://linuxhint.com/install_debian10_virtualbox/)
5. Attach the VM to the "internal network" called `internal_LAN`
6. Update the IP and gateway 
    * ip: 192.168.250.10/24
    * gateway: 192.168.250.2
7. check that `ping 8.8.8.8` works

Change to virtual gateway

8. Update the IP and gateway 
    * ip: 192.168.250.10/24
    * gateway: 192.168.250.1
9. check that `ping 8.8.8.8` works


Please note that there are no DHCP, not DNS on the `internal_LAN` subnet.


References:

* https://www.openbsd.org/faq/pf/example1.html
* https://www.openbsd.org/faq/pf/carp.html
* http://www.kernel-panic.it/openbsd/carp/carp4.html
* https://thefreecountry.wordpress.com/2018/11/27/configuring-snmp-v3-on-openbsd-6-4/